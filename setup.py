# -*- coding: utf-8 -*-
# Copyright (C) 2018  Duncan Macleod
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup

version = "1.0.4"

setup(
  name="ligo-common",
  version=version,
  maintainer="Duncan Macleod",
  maintainer_email="duncan.macleod@ligo.org",
  description="Empty LIGO modules",
  long_description="Empty module placeholder for other LIGO modules",
  license='GPLv3',
  provides=['ligo'],
  packages=['ligo'],
  setup_requires=['setuptools'],
  install_requires=['setuptools'],
  requires=['setuptools'],
  classifiers=[
      'Development Status :: 5 - Production/Stable',
      'Programming Language :: Python',
      'Programming Language :: Python :: 2.7',
      'Programming Language :: Python :: 3.4',
      'Programming Language :: Python :: 3.5',
      'Programming Language :: Python :: 3.6',
      'Programming Language :: Python :: 3.7',
      'Intended Audience :: Science/Research',
      'Intended Audience :: End Users/Desktop',
      'Intended Audience :: Developers',
      'Natural Language :: English',
      'Topic :: Scientific/Engineering',
      'Topic :: Scientific/Engineering :: Astronomy',
      'Topic :: Scientific/Engineering :: Physics',
      'Operating System :: OS Independent',
      'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
  ]
)
