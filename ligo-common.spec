%define name ligo-common
%define version 1.0.4
%define release 2

Summary: Empty LIGO modules
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
Source0: %{name}-%{version}.tar.gz
License: GPL
Group: Development/Libraries
Prefix: %{_prefix}
BuildArch: noarch
BuildRequires: rpm-build
BuildRequires: python-rpm-macros
BuildRequires: python2-rpm-macros
BuildRequires: python3-rpm-macros
BuildRequires: python-setuptools
BuildRequires: python%{python3_version_nodots}-setuptools
BuildRequires: python%{python3_other_version_nodots}-setuptools
Vendor: Duncan Macleod <duncan.macleod@ligo.org>

%description
Empty module placeholder for other LIGO modules

# -- python2-ligo-common

%package -n python2-%{name}
Obsoletes: %{name}
Provides: %{name}
Conflicts: %{name}
Summary: %{summary}
Requires: python
Requires: python-setuptools

%{?python_provide:%python_provide python2-%{name}}

%description -n python2-%{name}
Empty module placeholder for other LIGO modules

# -- python3a-ligo-common

%package -n python%{python3_version_nodots}-%{name}
Summary: %{summary}
Requires: python%{python3_version_nodots}
Requires: python%{python3_version_nodots}-setuptools

%{?python_provide:%python_provide python%{python3_version_nodots}-%{name}}

%description -n python%{python3_version_nodots}-%{name}
Empty module placeholder for other LIGO modules

# -- python3b-ligo-common

%package -n python%{python3_other_version_nodots}-%{name}
Summary: %{summary}
Requires: python%{python3_other_version_nodots}
Requires: python%{python3_other_version_nodots}-setuptools

%{?python_provide:%python_provide python%{python3_other_version_nodots}-%{name}}

%description -n python%{python3_other_version_nodots}-%{name}
Empty module placeholder for other LIGO modules

# -- build steps

%prep
%setup -n %{name}-%{version}

%build
%py2_build
%py3_build

%install
%py2_install
mkdir -p %{buildroot}/%{python2_sitearch}/ligo/
cp %{buildroot}/%{python2_sitelib}/ligo/__init__.py \
   %{buildroot}/%{python2_sitearch}/ligo/__init__.py
%py3_install
mkdir -p %{buildroot}/%{python3_sitearch}/ligo/
cp %{buildroot}/%{python3_sitelib}/ligo/__init__.py \
   %{buildroot}/%{python3_sitearch}/ligo/__init__.py
%py3_other_install
mkdir -p %{buildroot}/%{python3_other_sitearch}/ligo/
cp %{buildroot}/%{python3_other_sitelib}/ligo/__init__.py \
   %{buildroot}/%{python3_other_sitearch}/ligo/__init__.py

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python2-%{name}
%license LICENSE
%{python2_sitelib}/*
%{python2_sitearch}/*

%files -n python%{python3_version_nodots}-%{name}
%license LICENSE
%{python3_sitelib}/*
%{python3_sitearch}/*

%files -n python%{python3_other_version_nodots}-%{name}
%license LICENSE
%{python3_other_sitelib}/*
%{python3_other_sitearch}/*

# -- changelog

%changelog
* Thu Apr 25 2019 Duncan Macleod <duncan.macleod@ligo.org> - 1.0.4-2
- added python3_other build for epel7

* Fri Feb 15 2019 Duncan Macleod <duncan.macleod@ligo.org> - 1.0.4-1
- added missing setuptools runtime dependenncy

* Fri May 11 2018 Duncan Macleod <duncan.macleod@ligo.org> - 1.0.3-3
- packaging update, provides python3 packages
